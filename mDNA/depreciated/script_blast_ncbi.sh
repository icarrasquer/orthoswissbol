# !/bin/bash
source /local/anaconda3/bin/activate /home/ines/envblast/

for i in `ls *fastq`
    do
    sample=`echo $i | sed -e 's/.fastq//g'`
    
    sed -n '1~4s/^@/>/p;2~4p' "$sample".fastq > "$sample".fasta
    awk '{print $0"\t>read"NR}' "$sample".fasta > "$sample"_table_corr
    awk '/^>/{print ">read" ++i; next}{print}' "$sample".fasta > "$sample"_read.fasta
    rm "$sample".fasta
    #bgzip "$sample".fastq
    blastn -query "$sample"_read.fasta -db /data/database/nt -evalue 1e-6 -outfmt 6 -out "$sample"_blast_output.txt
    done
    