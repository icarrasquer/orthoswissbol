#! /bin/bash

#Subset the fasta and gff3 files of swiss families
grep -Ff chiro_list  chironomidae.fasta | sed -e 's/>//g' > list_chironomidae_samples_complete
fastaselect.pl  chironomidae.fasta list_chironomidae_samples_complete > chironomidae_good.fasta
grep -Ff chiro_list chironomidae.gff3 | grep "CDS" > chironomidae_good.gff3 

#Transform the gff3 file to include the name
cut -f 1,2 chironomidae_good.gff3 > temp1
cut --complement -f 1,2,3 chironomidae_good.gff3 > temp3
awk -F "gene=" '{print $2}' chironomidae_good.gff3 | awk -F ";" '{print $1}' > temp2
paste temp1 temp2 temp3 > chironomidae_good.gff3
cat temp2 | sort | uniq | sed '1d' > mito_CDS
rm temp*


source /local/anaconda3/bin/activate /home/ines/envbedtools/


bedtools getfasta -name -fi chironomidae_good.fasta -bed chironomidae_good.gff3 -fo chironomidae_good_CDS.fasta



##Balanced subset of mitocondria after phylogenetic inference

echo "Gene" "Length" "Nb probes" > summary 
while read a
    do
    #extract genes for the selection of species
    grep -w "$a" chironomidae_good_CDS.fasta | sed 's/>//g'> temp

    fastaselect.pl chironomidae_good_CDS.fasta temp  > chironomidae_good_CDS_"$a".fasta

    if [ "$a" != ATP8 ]
        then
        
        #Prepare index for probes cut
        cp chironomidae_good_CDS_"$a".fasta temp_ref.fasta

        java -jar /home/ines/envpicard/share/picard-2.22.1-0/picard.jar CreateSequenceDictionary R=temp_ref.fasta O=temp_ref.dict
        samtools faidx temp_ref.fasta
        awk -v FS="\t" -v OFS="\t" '{print $1 FS "0" FS ($2-1)}' temp_ref.fasta.fai > temp_ref.bed
        awk -F ":" '{print $3"_"$1}' temp_ref.bed > temp_colnames
        paste temp_ref.bed temp_colnames > temp_ref2.bed
        
        #Cut and addition of promoter T7 (5' -3' CCCTATAGTGAGTCGTATTA)
        java -jar /home/ines/envpicard/share/picard-2.22.1-0/picard.jar BedToIntervalList I=temp_ref2.bed SD=temp_ref.dict O=chironomidae_good_CDS_"$a".interval_list
        java -jar /home/ines/envpicard/share/picard-2.22.1-0/picard.jar BaitDesigner R=chironomidae_good_CDS_"$a".fasta T=chironomidae_good_CDS_"$a".interval_list DESIGN_NAME=chironomidae_good_CDS_"$a"_probes_folder BAIT_SIZE=130 RIGHT_PRIMER=CCCTATAGTGAGTCGTATTA LEFT_PRIMER=null BAIT_OFFSET=130
        cat chironomidae_good_CDS_"$a"_probes_folder/chironomidae_good_CDS_"$a"_probes_folder.design.fasta >> chironomidae_good_CDS_probes.fasta
        rm temp_*

        NB_PB=`grep -c ">" chironomidae_good_CDS_"$a"_probes_folder/chironomidae_good_CDS_"$a"_probes_folder.design.fasta`



    else

        sh ./script_addT7.sh chironomidae_good_CDS_"$a".fasta
        cat chironomidae_good_CDS_probes.fasta chironomidae_good_CDS_"$a"_ready.fasta >> chironomidae_good_CDS_probes.fasta
    
        NB_PB=`grep -c ">" chironomidae_good_CDS_"$a"_ready.fasta`

    
    fi    
    ##Summary

    source /local/anaconda3/bin/activate /home/jeremy/local/envemboss/  
       

    LEN=`infoseq chironomidae_good_CDS_"$a".fasta |  awk '{print $6}' | awk 'NR == 3 {print; exit}'`

    
    echo "$a" "$LEN" "$NB_PB" >> summary

    done < mito_CDS

