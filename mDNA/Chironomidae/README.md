# Mitocondrial DNA


## Complete mitochondrion available for Chironomidae

| Species                    | Bp     | Accesion number |
|----------------------------|--------|-----------------|
| Propsilocerus taihuensis   | 16,083 | NC_059070.1     |
| Propsilocerus sinicus      | 16,002 | NC_059037.1     |
| Propsilocerus paradoxus    | 16,172 | NC_059036.1     |
| Propsilocerus akamusi      | 16,042 | NC_059035.1     |
| Paratrichocladius tamaater | 16,441 | NC_059034.1     |
| Polypedilum vanderplanki   | 16,445 | NC_028015.1     |
| Rheocricotopus villiculus  | 15,985 | NC_060345.1     |
| Prodiamesa olivacea        | 16,190 | NC_060344.1     |
| Clinotanypus yani          | 16,247 | NC_060343.1     |
| Tanypus punctipennis       | 16,215 | NC_058587.1     |
| Parochlus steinenii        | 16,803 | NC_027591.1     |
| Chironomus tepperi         | 15,652 | NC_016167.1     |


Chosen ones:
NC_026015
NC_060345
NC_059070
NC_059036
NC_060343
NC_060344
