#! /bin/bash

for i in Rana_temporaria_mitochondion_anno.fasta
    do
    file_name=`echo $i | sed -e 's/.fasta//g'`      
#Prepare index for probes cut
    sed '/^[^>]/s/[R|Y|W|S|M|K|H|B|V|D|N]//g' "$i" > temp_ref.fasta

    java -jar /home/ines/envpicard/share/picard-2.22.1-0/picard.jar CreateSequenceDictionary R=temp_ref.fasta O=temp_ref.dict
    samtools faidx temp_ref.fasta
    awk -v FS="\t" -v OFS="\t" '{print $1 FS "0" FS ($2-1)}' temp_ref.fasta.fai > temp_ref.bed
    awk -F ":" '{print $3"_"$1}' temp_ref.bed > temp_colnames
    paste temp_ref.bed temp_colnames > temp_ref2.bed
            
    #Cut and addition of promoter T7 (5' -3' CCCTATAGTGAGTCGTATTA)
    java -jar /home/ines/envpicard/share/picard-2.22.1-0/picard.jar BedToIntervalList I=temp_ref2.bed SD=temp_ref.dict O="$file_name".interval_list
    java -jar /home/ines/envpicard/share/picard-2.22.1-0/picard.jar BaitDesigner R=temp_ref.fasta T="$file_name".interval_list DESIGN_NAME="$file_name"_probes_folder RIGHT_PRIMER=CCCTATAGTGAGTCGTATTAAGAT LEFT_PRIMER=GTGACTGGAGTTCAGACG BAIT_OFFSET=158 BAIT_SIZE=158 # max =200 bp - (18+24) =200 -42 
    cat "$file_name"_probes_folder/"$file_name"_probes_folder.design.fasta > "$file_name"_probes.fasta
    rm temp_*
    sed '/^[^>]/s/[R|Y|W|S|M|K|H|B|V|D|N]//g' 

    done

