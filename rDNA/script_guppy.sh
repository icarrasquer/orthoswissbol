#! /bin/bash

/data/work/Ortho_SwissBol/rDNA/MinION_ampliconSEQ/Guppy/
guppy_basecaller -i /data/work/Ortho_SwissBol/rDNA/MinION_ampliconSEQ/Guppy/fast5 -s /data/work/Ortho_SwissBol/rDNA/MinION_ampliconSEQ/Guppy/fasta - --flowcell FLO-MIN106 --kit SQK-LSK110 --compress_fastq
