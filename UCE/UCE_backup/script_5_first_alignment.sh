# !/bin/bash
#Folder_ aligment

source /local/anaconda3/bin/activate /home/ines/envrmaptools
export cores=16
export base=Teleo_oc 
export base_dir=/data/work/Ortho_SwissBol/UCE_genomes/Main/alignment
for critter in Ache_do Apte_asa Gry_bi Copto_for Laupa_ko Locus_mi Teleo_oc Vandi_vi Xeno_bra;
    do
        export reads=$critter-reads.fq.gz;
        mkdir -p $base_dir/$critter;
        cd $base_dir/$critter;
        /home/jeremy/local/stampy/stampy.py --maxbasequal 93 -g ../../base/$base -h ../../base/$base \
        --substitutionrate=0.05 -t$cores --insertsize=400 -M \
        ../../reads/$reads | samtools view -Sb - > $critter-to-$base.bam;
    done

