#! /bin/bash

#Add T7 promoter

for i in `$@`
    do
    sample=`echo $i | sed -e 's/.fasta//g'`
    awk '/^>/ {printf("\n%s\n",$0);next; } { printf("%s",$0);}  END {printf("\n");}' < "$i" | sed '/^>/ !{s/$/CCCTATAGTGAGTCGTATTA/}' | sed '1d' > "$sample"_ready.fasta
    sed '/^[^>]/s/[R|Y|W|S|M|K|H|B|V|D|N]/+/g' "$sample"_ready.fasta > temp
    VAR=`grep -c "+" temp`
    echo "nb of non regular bases" $VAR
    rm temp
    done
